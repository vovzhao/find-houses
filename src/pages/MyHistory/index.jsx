import React from "react";
import { Link } from "react-router-dom";
import { BASE_URL } from "../../utils/url";
import HouseItem from "../../components/HouseItem";
import MyNavBar from "../../components/MyNavBar";
import NoHouse from "../../components/NoHouse";
import styles from "./index.module.css";
import { getHistory } from "../../utils/history";

export default function MyFavorite(props) {
  const list = getHistory() || [];

  const renderHouseItem = () => {
    const { history } = props;
    return list.map((item) => (
      <HouseItem
        key={item.houseCode}
        src={BASE_URL + item.houseImg}
        onClick={() => history.push(`/detail/${item.houseCode}`)}
        title={item.title}
        desc={item.desc}
        tags={item.tags}
        price={item.price}
      />
    ));
  };

  const renderHouseList = () => {
    if (list.length === 0) {
      return (
        <NoHouse>
          您还没有浏览房源,
          <Link to="/home/list" className={styles.link}>
            {" "}
            去看看房源吧~
          </Link>
        </NoHouse>
      );
    } else {
      return (
        <div className={styles.houses}>
          {renderHouseItem()}
          <p className={styles.showAll}>共{list.length}项，已加载全部</p>
        </div>
      );
    }
  };
  return (
    <div className={styles.root}>
      <MyNavBar onLeftClick={() => props.history.goBack()}>看房记录</MyNavBar>
      {renderHouseList()}
    </div>
  );
}
