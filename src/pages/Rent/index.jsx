import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import API from "../../utils/api";
import { BASE_URL } from "../../utils/url";
import HouseItem from "../../components/HouseItem";
import MyNavBar from "../../components/MyNavBar";
import NoHouse from "../../components/NoHouse";
import styles from "./index.module.css";
import { Toast } from "antd-mobile";

export default function MyFavorite(props) {
  const [list, setlist] = useState([]);
  const [isLoading, setisLoading] = useState(false);
  const { history, location } = props;

  // 获取已收藏房源列表信息
  useEffect(() => {
    const getHouseList = async () => {
      setisLoading(true);
      Toast.loading("Loading...", 0, null, false);
      const res = await API.get("/user/houses");
      Toast.hide();
      setisLoading(false);
      const { status, body } = res.data;
      if (status === 200) {
        setlist(body);
      } else {
        history.replace("/login", {
          from: location,
        });
      }
    };
    getHouseList();
  }, [history, location]);

  const renderHouseItem = () => {
    const { history } = props;
    return list.map((item) => (
      <HouseItem
        key={item.houseCode}
        src={BASE_URL + item.houseImg}
        onClick={() => history.push(`/detail/${item.houseCode}`)}
        title={item.title}
        desc={item.desc}
        tags={item.tags}
        price={item.price}
      />
    ));
  };

  const renderHouseList = () => {
    if (list.length === 0 && !isLoading) {
      return (
        <NoHouse>
          您还没有发布房源,
          <Link to="/home/list" className={styles.link}>
            {" "}
            去发布房源吧~
          </Link>
        </NoHouse>
      );
    } else {
      return (
        <div className={styles.houses}>
          {renderHouseItem()}
          {list.length ? (
            <p className={styles.showAll}>共{list.length}项，已加载全部</p>
          ) : null}
        </div>
      );
    }
  };
  return (
    <div className={styles.root}>
      <MyNavBar onLeftClick={() => props.history.goBack()}>我的发布</MyNavBar>
      {renderHouseList()}
    </div>
  );
}
