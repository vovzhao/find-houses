import React, { useState, useEffect } from "react";
import { Carousel, Flex, Modal, Toast } from "antd-mobile";
import MyNavBar from "../../components/MyNavBar";
import HousePackage from "../../components/HousePackage";
import { BASE_URL } from "../../utils/url";
import { isAuth } from "../../utils/auth";
import API from "../../utils/api";
import styles from "./index.module.css";

const BMapGL = window.BMapGL;
// 覆盖物样式
const labelStyle = {
  cursor: "pointer",
  border: "0px solid rgb(255, 0, 0)",
  padding: "0px",
  whiteSpace: "nowrap",
  fontSize: "14px",
  color: "rgb(255, 255, 255)",
  textAlign: "center",
};

const alert = Modal.alert;

const obj = {
  houseImg: [],
  title: "",
  tags: [],
  price: 0,
  // 房型
  roomType: "",
  // 房屋面积
  size: 0,
  // 朝向
  oriented: [],
  // 楼层
  floor: "",
  // 小区名称
  community: "",
  // 地理位置
  coord: {
    latitude: "39.928033",
    longitude: "116.529466",
  },
  // 房屋配套
  supporting: [],
  // 房屋标识
  houseCode: "",
  // 房屋描述
  description: "",
};

export default function HouseDetail(props) {
  // 从路由参数中获取房屋id
  const { id } = props.match.params;
  const [isFavorite, setisFavorite] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [homeInfo, sethomeInfo] = useState(obj);
  // 功能已关闭
  const closed = () => {
    Toast.info("暂未开通该功能", 2, null, false);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // 获取房屋详情数据
  useEffect(() => {
    const getHouseDetail = async () => {
      setisLoading(true);
      const res = await API.get(`/houses/${id}`);
      sethomeInfo(res.data.body);
      setisLoading(false);
      // 获取数据，渲染地图
      const { community, coord } = res.data.body;
      renderMap(community, coord);
    };
    getHouseDetail();
  }, [id]);

  // 检查收藏状态
  useEffect(() => {
    const checkFavorite = async () => {
      const isLogin = isAuth();
      if (isLogin) {
        // 查询房屋是否被收藏
        const res = await API.get(`/user/favorites/${id}`);

        const { status, body } = res.data;
        if (status === 200) {
          // 请求成功
          setisFavorite(body.isFavorite);
        }
      } else {
        return;
      }
    };
    checkFavorite();
  }, [id]);

  // 渲染轮播图组件
  const renderSwipers = () => {
    const { houseImg } = homeInfo;
    return houseImg.map((item) => (
      <img key={item} src={BASE_URL + item} alt="房屋图片" />
    ));
  };

  // 渲染地图
  const renderMap = (community, coord) => {
    const { longitude, latitude } = coord;
    const map = new BMapGL.Map("map");
    const point = new BMapGL.Point(longitude, latitude);
    map.centerAndZoom(point, 17);
    // 添加缩放控件
    map.addControl(new BMapGL.ZoomControl());

    // 创建覆盖物
    const label = new BMapGL.Label("", {
      position: point,
      offset: new BMapGL.Size(0, 0),
    });

    // 设置房源覆盖物内容
    label.setContent(`
            <div class="${styles.rect}">
                <span class="${styles.housename}">${community}</span>
                <i class="${styles.arrow}"></i>
            </div>
        `);

    // 设置样式
    label.setStyle(labelStyle);

    // 添加覆盖物到地图中
    map.addOverlay(label);
  };

  // 收藏
  const handleFavorite = async () => {
    const isLogin = isAuth();
    const { history, location, match } = props;

    if (isLogin) {
      // 已登录
      const { id } = match.params;

      if (isFavorite) {
        // 取消收藏
        const res = await API.delete(`/user/favorites/${id}`);
        setisFavorite(false);
        if (res.data.status === 200) {
          Toast.info("已取消收藏", 1, null, false);
        } else {
          Toast.info("请求超时，请稍后再试", 2, null, false);
        }
      } else {
        // 添加收藏
        const res = await API.post(`/user/favorites/${id}`);

        if (res.data.status === 200) {
          Toast.info("收藏成功", 1, null, false);
          setisFavorite(true);
        } else {
          Toast.info("请求超时，请稍后再试", 2, null, false);
        }
      }
    } else {
      // 未登录
      return alert("提示", "该功能需要先登录，是否登录？", [
        { text: "否" },
        {
          text: "是",
          onPress: () => history.push("/login", { from: location }),
        },
      ]);
    }
  };

  const {
    community,
    title,
    tags,
    price,
    roomType,
    size,
    floor,
    oriented,
    supporting,
    description,
  } = homeInfo;
  return (
    <div className={styles.root}>
      {/* 导航 */}
      <MyNavBar
        className={styles.navHeader}
        rightContent={[
          <i key="share" className="iconfont icon-share" onClick={closed} />,
        ]}
      >
        {community}
      </MyNavBar>

      {/* 轮播图 */}
      <div className={styles.slides}>
        {!isLoading ? (
          <Carousel autoplay infinite autoplayInterval={2000}>
            {renderSwipers()}
          </Carousel>
        ) : null}
      </div>

      {/* 房屋基本信息 */}
      <div className={styles.info}>
        <h3 className={styles.infoTitle}>{title}</h3>
        <Flex className={styles.infoTags}>
          <Flex.Item>
            {tags.map((item, index) => (
              <span
                key={item}
                className={[
                  styles.tag,
                  styles.tags,
                  styles["tag" + ((index % 4) + 1)],
                ].join(" ")}
              >
                {item}
              </span>
            ))}
          </Flex.Item>
        </Flex>

        <Flex className={styles.infoPrice}>
          <Flex.Item className={styles.infoPriceItem}>
            <div>
              {price}
              <span className={styles.month}>/月</span>
            </div>
            <div>租金</div>
          </Flex.Item>
          <Flex.Item className={styles.infoPriceItem}>
            <div>{roomType}</div>
            <div>房型</div>
          </Flex.Item>
          <Flex.Item className={styles.infoPriceItem}>
            <div>{size}平方</div>
            <div>面积</div>
          </Flex.Item>
        </Flex>

        <Flex className={styles.infoBasic} align="start">
          <Flex.Item>
            <div>
              <span className={styles.title}>装修：</span>
              精装
            </div>
            <div>
              <span className={styles.title}>楼层：</span>
              {floor}
            </div>
          </Flex.Item>
          <Flex.Item>
            <div>
              <span className={styles.title}>朝向：</span>
              {oriented.join("、")}
            </div>
            <div>
              <span className={styles.title}>类型：</span>
              普通住宅
            </div>
          </Flex.Item>
        </Flex>
      </div>

      {/* 地图位置 */}
      <div className={styles.map}>
        <div className={styles.mapTitle}>
          小区：
          <span>{title}</span>
        </div>
        <div className={styles.mapContainer} id="map">
          地图
        </div>
      </div>

      {/* 房屋配套 */}
      <div className={styles.about}>
        <div className={styles.houseTitle}>房屋配套</div>
        {supporting.length === 0 ? (
          <div className={styles.titleEmpty}>暂无数据</div>
        ) : (
          <HousePackage list={supporting} />
        )}
      </div>

      {/* 房屋概况 */}
      <div className={styles.set}>
        <div className={styles.houseTitle}>房屋概况</div>
        <div>
          <div className={styles.contact}>
            <div className={styles.user}>
              <img src={BASE_URL + "/img/avatar.png"} alt="头像" />
              <div className={styles.userInfo}>
                <div>张女士</div>
                <div className={styles.userAuth}>
                  <i className="iconfont icon-auth" />
                  已认证房主
                </div>
              </div>
            </div>
            <span className={styles.userMsg}>发消息</span>
          </div>
          <div className={styles.descText}>{description || "暂无房屋数据"}</div>
        </div>
      </div>
      {/* 底部按钮 */}
      <div className={styles.buttons}>
        <span className={styles.favorite} onClick={handleFavorite}>
          <img
            src={BASE_URL + (isFavorite ? "/img/star.png" : "/img/unstar.png")}
            alt="收藏"
          />
          {isFavorite ? "已收藏" : "收藏"}
        </span>
        <span className={styles.consult} onClick={closed}>
          在线咨询
        </span>
        <span className={styles.reserve}>
          <a href="tel: 183614999905">电话预定</a>
        </span>
      </div>
    </div>
  );
}
