import React from "react";
import { Flex } from "antd-mobile";
import styles from "./index.module.css";

export default function FilterFooter({
  cancelText,
  okText,
  onCancel,
  onOk,
  className,
}) {
  return (
    <Flex className={[styles.root, className || ""].join(" ")}>
      {/* 取消按钮 */}
      <span
        className={[styles.btn, styles.cancel].join(" ")}
        onClick={onCancel}
      >
        {cancelText ? cancelText : "取消"}
      </span>

      {/* 确定按钮 */}
      <span className={[styles.btn, styles.ok].join(" ")} onClick={onOk}>
        {okText ? okText : "确定"}
      </span>
    </Flex>
  );
}
