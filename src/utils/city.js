const CITY = "zfy_city";

const getCity = () => JSON.parse(localStorage.getItem(CITY));

export { getCity };
